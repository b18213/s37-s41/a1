// [SECTION] DEPENDENCIES
const express = require('express');
const router = express.Router();

// [SETION] IMPORTED MODULES
const userController = require('../controllers/userController')

const auth = require('../auth');

// object destructuring from auth module

const {verify, verifyAdmin} = auth;

// [SECTION] ROUTES
router.post('/', userController.registerUser);

// get all user
/*
	Mini Activity
	>> endpoint:'/'
	Create a route for getting all users
	Use the appropriate HTTP method
*/

router.get('/', userController.getAllUsers);

// login user

router.post('/login',userController.loginUser);

router.get('/getuserDetails',verify,  userController.getUserDetails)

// Activity 3 || checkEmailExists
router.post('/checkEmailExists', userController.checkEmailExists);
// Updating user details

router.put('/updateUserDetails', verify, userController.updateUserDetails)

/*
	Mini-Activity
		>>Create a user route which is able to capture the id from its url using route params
			-- This route will only update a regular user to an admin
			--Only an admin can access this route
			--Test the route in postman
			--Send your outputs in Hangouts
*/

router.put('/updateAdmin/:id', verify, verifyAdmin, userController.updateAdmin);

router.post('/enroll', verify, userController.enroll);

router.get('/getEnrollments', verify, userController.getEnrollments);

module.exports = router;
