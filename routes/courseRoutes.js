// DEPENDENCIES

const express = require('express');
const router = express.Router();

// IMPORTED MODULES
const courseController = require('../controllers/courseController')

const auth = require('../auth')

const {verify, verifyAdmin} = auth;

// ROUTES
router.post('/', verify, verifyAdmin, courseController.addNewCourse);

router.get('/', courseController.getAllCourses);

// Activity 3 || getSingleCourse

router.get('/getSingleCourse/:id', courseController.getSingleCourse);

// update a course
router.put('/:id', verify, verifyAdmin, courseController.updateCourse)

// Activity 4 || ARCHIVE COURSE
	
router.put('/archive/:id', verify, verifyAdmin, courseController.archiveCourse);

// Activity 4 || ACTIVATE COURSE
	
router.put('/activate/:id', verify, verifyAdmin, courseController.activateCourse);

// Activity 4 || RETRIEVE ALL ACTIVE COURSES
router.get('/getActiveCourses', courseController.getAllActiveCourses);

// Find courses by name
router.post('/findCoursesByName', courseController.findCoursesByName);

module.exports = router;