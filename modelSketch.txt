App: Booking System API
Description: Allows user to enroll to a course. Allows Admin to perform CRUD operations on courses. Allows regular users to register.

user
firstName - string,
lastName - string,
email - string,
password - string,
mobileNo - string,
isAdmin - boolean,
			default: false
enrollments: [
	{
		courseId: string,
		status: string,
		dateEnrolled: date
	}
]

** all values in the field given should be required, all default values should be followed.

Course
name -string,
description - string,
price- number,
isActive - boolean
		default: true
createdOn: date
enrollees: [
	{
		userId: string,
		status: string,
		dateEnrolled: date
	}

]

** all values in the field given should be required, all default values should be followed.