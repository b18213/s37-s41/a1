// DEPENDENCIES

const Course = require('../models/course');


// ADD NEW COURSE

module.exports.addNewCourse = (req,res) => {

	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	});

	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));
};

// GET ALL COURSES
module.exports.getAllCourses = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

// ACTIVITY 3 || GET SINGLE COURSE DETAILS
module.exports.getSingleCourse = (req, res) => {

	console.log(req.params);
	
	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// [SECTION] UPDATNIG A COURSE

module.exports.updateCourse = (req, res) =>{

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};

// ACTIVTY 4 || ARCHIVE COURSE
 
 module.exports.archiveCourse = (req, res) => {

 	let updates = {
 		isActive : false
 	};

 	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
 	.then(updatedCourse => res.send(updatedCourse))
 	.catch(err => res.send(err));

 };

 // ACTIVITY 4 || ACTIVATE COURSE

 module.exports.activateCourse = (req, res) =>{

 	let updates = {
 		isActive : true
 	};

 	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
 	.then(updatedCourse => res.send(updatedCourse))
 	.catch(err => res.send(err));
 };

 // ACTIVTY 4 || RETRIEVE ALL ACTIVE COURSES

 module.exports.getAllActiveCourses = (req, res) =>{

 	Course.find({isActive: true})
 	.then(result => res.send(result))
 	.catch(err => res.send(err));
 };

 // [SECTION] FIND COURSES BY NAME

 module.exports.findCoursesByName = (req, res) =>{

 	console.log(req.body) //contain the name of the course you are looking for

 	Course.find({name: {$regex: req.body.name, $options: '$i'}}).then(result =>{

 		if(result.length === 0){
 			
 			return res.send('No courses found')

 		}else{

 			return res.send(result)
 		}
 	})
 };
