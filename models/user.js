const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required"]
	},

	lastName: {
		type: String,
		required: [true, "Last name is requried"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	enrollments: [
		{
			courseId:{
				type: String,
				required: [true, "Course Id is required"]
			},

			status:{
				type: String,
				default: "Enrolled"
			},

			dateEnrolled:{
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);