// [SECTION] DEPENDENCIES

const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	console.log(user);

	// data object is created to contain some details of user
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	console.log(data);

	return jwt.sign(data, secret, {})
} 

// NOTES:
/*
	1. You can only get access token when a user logs in in your app with the correct credentials.

	2. As a user, you can only get your own details from your own token form logging in.

	3. JWT is not meant for sensitive data.

	4. JWT is like a passport, you use around the app to access certain features meant for your type of user..
*/

// goal: to check if the token exists or it is not tampered

module.exports.verify = (req, res, next ) => {

	// req.headers.authorization - contains jwt/token
	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		// type of result is a string 

		return res.send({auth: "Failed. No Token"})
	} else {

		/*
			Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWE0OGMwODVjMTNmOGQ3OTk5ZiIsImVtYWlsIjoiTVJhaWRlbkBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NTQxMzIxMTF9.w5YiLOYyWb_Jz9A6wT4fSyGogPD6Jv1W5XPn8XA7eT4
		*/
		token = token.slice(7, token.length)
		console.log(token)

		jwt.verify(token, secret, (err, decodedToken) => {

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else{

				req.user = decodedToken

				// will let us proceed to the next middleware or controller
				next();
			}	
		})
	}
};

// verifying an admin

module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){
		next();
	}else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
};